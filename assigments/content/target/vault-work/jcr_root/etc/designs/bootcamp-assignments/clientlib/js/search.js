// app.controller('searchController', function($scope, $http) {
//
//     var offset       		= 0;
//     var locale       		= '';
//     var limit        		= 0;     // result items per page
//     var navPageMax          = 5;     // maximum number of pages to display on pager
//     var pageCount           = 0;
//     var searchResultTextTemplate   = '';
//     var noSearchResultTextTemplate = '';
//
//     $scope.currentPageNr    = 1;
//     $scope.resultItems 		= new Array();
//     $scope.navPageNumbers   = new Array();
//     $scope.total 	 		= 0;
//     $scope.searchStr 		= '';
//
//     function getResults() {
//
//         searchUrl = "/bin/corporate/search?" +
//                     "search=" + $scope.searchStr +
//                     "&locale=" + locale +
//                     "&offset=" + offset;
//
//         if (limit>0) {
//             searchUrl = searchUrl + "&resultsPerPage=" + limit;
//         }
//
//         $http.get(searchUrl).success(function(response) {
//             $scope.total = response.total;
//           	$scope.resultItems = response.hits;
//
//             if ($scope.total==0) {
//                 var labelStr = noSearchResultTextTemplate;
//                 labelStr = labelStr.replace("[0]", $scope.searchStr);
//                 $("#noResultLabel").html(labelStr);
//             } else {
//                 var labelStr = searchResultTextTemplate;
//                 labelStr = labelStr.replace("[0]", decodeURI($scope.searchStr));
//                 labelStr = labelStr.replace("[1]", $scope.total);
//                 $("#resultLabel").html(labelStr);
//             }
//
//             $scope.navPageNumbers = getNavPageNumbers();
//       });
//     }
//
//     function getNavPageNumbers() {
//     	var pageNumbers = new Array();
//     	if ($scope.total>0) {
//         	pageCount = Math.ceil($scope.total / limit);
//         	for (var i = 1; i <= pageCount && i<=navPageMax; i++) {
//         		pageNumbers.push(i);
//         	}
//     	}
//     	return pageNumbers;
//     }
//
//     function getRequestParam(paramName) {
//         var paramValue = '';
//         var requestParamStr = location.search;
//
//         // get search param from request params
//         if (requestParamStr.indexOf("?")==0) {
//             requestParamStr = requestParamStr.slice(1);
//         }
//         var params = requestParamStr.split("&");
//         for (var i = 0; i < params.length; i++) {
//             var pair = params[i].split('=');
//             if (pair[0]==paramName) {
//                paramValue = pair[1];
//                break;
//             }
//         }
//         return paramValue;
//     }
//
//     $scope.search = function(searchInput) {
//         offset = 0;
//         $scope.currentPageNr = 1;
//         $scope.resultItems = new Array();
//         $scope.searchStr = encodeHTML(searchInput);
//         getResults();
//     }
//
//     function encodeHTML(inputStr) {
//         return inputStr.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
//     }
//
//     $scope.navPageLinkClicked = function(pageNr) {
//         $scope.currentPageNr = pageNr;
//         offset = (($scope.currentPageNr - 1) * limit);
//         getResults();
//     }
//
//     $scope.navPrevClicked = function() {
//     	$scope.currentPageNr = $scope.currentPageNr - 1;
//         offset = (($scope.currentPageNr - 1) * limit);
//         getResults();
//     }
//
//     $scope.navNextClicked = function() {
//     	$scope.currentPageNr = $scope.currentPageNr + 1;
//         offset = (($scope.currentPageNr - 1) * limit);
//         getResults();
//     }
//
//     $scope.hasPrevPage = function() {
//     	return ($scope.currentPageNr > 1) ? true : false;
//     }
//
//     $scope.hasNextPage = function() {
//     	return ($scope.currentPageNr < pageCount) ? true : false;
//     }
//
//     $scope.showPager = function() {
//     	return (pageCount > 1) ? true : false;
//     }
//
//     $scope.init = function(pLimit, pLocale) {
//         $scope.searchStr = getRequestParam('search');
//         $scope.searchStr = encodeHTML($scope.searchStr);
//
//         searchResultTextTemplate = $('#resultLabel').html();
//         noSearchResultTextTemplate = $('#noResultLabel').html();
//
//         locale = pLocale;
//         limit  = pLimit;
//         limit  = (!$.isNumeric(limit)) ? 0 : parseInt(limit);
//
//         getResults();
//     }
//
// });
