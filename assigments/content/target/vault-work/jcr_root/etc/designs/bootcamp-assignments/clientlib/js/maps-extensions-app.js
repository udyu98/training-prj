//backup of modified map elements (used in contacts)
    
/*    
 	toggleContactLocations: function () {
        $('.contactmap-locations li').has('ul').addClass('js-is-closed');
        $('.contactmap-locations ul li ul').siblings('p').on('click', function() {
            var liInUl = $(this).parent('li');
            if (!liInUl.hasClass('js-is-open')) {
                liInUl.removeClass('js-is-closed').addClass('js-is-open');
            } else {
                liInUl.removeClass('js-is-open').addClass('js-is-closed');
            }

            if(contactGMap != null) {
                google.maps.event.trigger(contactGMap, 'resize');
            }

        });  
    },
    
    toggleContactDetails: function () {
        $('.contactmap-locations li p').on('click', function(){
            var locationsListItem = $(this).closest('li');
            if (!locationsListItem.hasClass('active')) {
                locationsListItem.siblings().removeClass('active');
                locationsListItem.addClass('active');
                locationsListItem.parents('.js-is-closed').removeClass('js-is-closed').addClass('js-is-open');
            } else {
                locationsListItem.removeClass('active');
            }
            $('.contactmap-locations li').not(locationsListItem).removeClass('active');
            
            var contactmapDetails = locationsListItem.closest('.contactmap-locations').siblings('.contactmap-details').find('li');
            var target = locationsListItem.data('href');
            contactmapDetails.removeClass('js-is-open').addClass('js-is-closed');
            $(target).removeClass('js-is-closed').addClass('js-is-open');

            
            if(contactGMap != null) {
            	var longitude = $(target).data('longitude');
            	var latitude = $(target).data('latitude');
	            if(longitude != null && latitude != null) {
	            	longitude = parseFloat(longitude);
	            	latitude = parseFloat(latitude);
	            	
	                var officeLocation = {lat: latitude, lng: longitude};
	            	var mapsPosition;
	            	if(longitude < parseFloat("180")) {
	                	mapsPosition = {lat: latitude, lng: (longitude+parseFloat("0.0020"))};
	            	} else {
	            		mapsPosition = {lat: latitude, lng: (longitude)};
	            	}
	                
	                var image = '/etc/designs/bootcamp-assignments/clientlib/images/marker.png';
	               
	                contactGMap.panTo(mapsPosition);
                	contactGMap.setZoom(16);

	                if(contactGMarker != null) {
	                	contactGMarker.setMap(null);
	                }
	                
	                if(contactGDropPin == "drop") {
		                contactGMarker = new google.maps.Marker({
		                    animation: google.maps.Animation.DROP,
		                    position: officeLocation,
		                    map: contactGMap,
		                    icon: image
		                });
	                } else if(contactGDropPin == "bounce") {
		                contactGMarker = new google.maps.Marker({
		                    animation: google.maps.Animation.BOUNCE,
		                    position: officeLocation,
		                    map: contactGMap,
		                    icon: image
		                });
	                } else {
	                	contactGMarker = new google.maps.Marker({
		                    position: officeLocation,
		                    map: contactGMap,
		                    icon: image
		                });
	                }
            	} else {
                    if(contactGMap != null) {
                    	contactGMap.setZoom(2);
                    }

                    if(contactGMarker != null) {
                    	contactGMarker.setMap(null);
                    }
            	}
            }
        });
    },*/
